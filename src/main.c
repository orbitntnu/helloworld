// NOTE: Hello world is of course not copyrighted, this is for copy paste
// purposes! You may ignore the licence in this special case. If you use
// this as a template for your own project, change the name in the licence
// and remove this top note.

/*
 * Copyright (C) 2018 Erlend Sveen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#include <stdio.h>

int main (int argc, char **argv)
{
	printf ("Hello, World!\n");
	return 0;
}
