###############################################################################
# Project name and files
###############################################################################
PROJECT_NAME = helloworld
PROJECT_FILES = $(shell find src -name "*.*")

NOREL_BASE_TEXT   = 0x0800D800
NOREL_BASE_RODATA = 0x0800E800
NOREL_BASE_DATA   = 0x0800F000
NOREL_BASE_BSS    = 0x0800F42C

LIB = ../../lib/

USR_INC = -I$(LIB)libuser/inc
USR_LIB = -L$(LIB)libuser/build
USR_LIBS = -lcglue -luser

###############################################################################
# Compiler flags, file processing and makefile execution
###############################################################################
include ../../util/build/makefile/flags
include ../../util/build/makefile/processfiles
include ../../util/build/makefile/targets
